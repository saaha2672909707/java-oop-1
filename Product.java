public class Product {
    private int price;
    private String color;
    private int weight;

    public Product(int price, int weight, String color){
        this.price = price;
        this.weight = weight;
        this.color = color;
    }

    @Override
    public String toString() {
        return "Product{" +
                "price=" + price +
                ", color='" + color + '\'' +
                ", weight=" + weight +
                '}';
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getPrice() {
        return price;
    }

    public int getWeight() {
        return weight;
    }

    public String getColor() {
        return color;
    }
}
