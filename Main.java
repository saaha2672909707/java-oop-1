public class Main {
    public static void main(String[] args) {
        Product apple = new Product(10, 23, "read");
        Product cucumber = new Product(21, 56, "green");
        System.out.println(apple);
        System.out.println(cucumber);
    }
}